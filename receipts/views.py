from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import View
from .forms import ReceiptForm, ExpenseCategoryForm, AccountForm


# Create your views here.


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts": receipts}
    return render(request, "receipts/receipt_list.html", context)


class CreateReceipt(LoginRequiredMixin, View):
    def get(self, request):
        form = ReceiptForm()
        context = {"form": form}
        return render(request, "receipts/create_receipt.html", context)

    def post(self, request):
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
        context = {"form": form}
        return render(request, "receipts/create_receipt.html", context)


class ExpenseCategoryList(LoginRequiredMixin, View):
    def get(self, request):
        expense_categories = ExpenseCategory.objects.filter(owner=request.user)
        context = {"expense_categories": expense_categories}
        return render(request, "receipts/expense_category_list.html", context)


class AccountList(LoginRequiredMixin, View):
    def get(self, request):
        accounts = Account.objects.filter(owner=request.user)
        context = {"accounts": accounts}
        return render(request, "receipts/account_list.html", context)


class CreateExpenseCategory(LoginRequiredMixin, View):
    def get(self, request):
        form = ExpenseCategoryForm()
        context = {"form": form}
        return render(request, "receipts/create_category.html", context)

    def post(self, request):
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
        context = {"form": form}
        return render(request, "create_category.html", context)


class CreateAccount(LoginRequiredMixin, View):
    def get(self, request):
        form = AccountForm()
        context = {"form": form}
        return render(request, "receipts/create_account.html", context)

    def post(self, request):
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
        context = {"form": form}
        return render(request, "receipts/create_account.html", context)
