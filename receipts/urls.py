from django.urls import path
from .views import (
    receipt_list,
    CreateReceipt,
    ExpenseCategoryList,
    AccountList,
    CreateExpenseCategory,
    CreateAccount,
)


urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", CreateReceipt.as_view(), name="create_receipt"),
    path("categories/", ExpenseCategoryList.as_view(), name="category_list"),
    path("accounts/", AccountList.as_view(), name="account_list"),
    path(
        "categories/create/",
        CreateExpenseCategory.as_view(),
        name="create_category",
    ),
    path("accounts/create/", CreateAccount.as_view(), name="create_account"),
]
